* Users Details for Registration
{
	isnewCustomer: boolean,
	firstName: string,
	lastName: string,
	mobileNumber: string
	address: string,
	email: string,
	password: string,
	reenterPassword: string,
	isAdmin: boolean
}
* Products
{
	 name: string,
	 description: string,
	 ismessageSeller: string,
	 isAddtoCart: boolean
	 orderDetails: [
	 	{
	 		userId: string,
			transactionDateTime: string,
	 		price: number,
	 		variation: string,
			quantity: string,
			stocks: string,
			isActive: boolean,
			orderStatus: string,
			total: number
	 	}
	 ],
}

* Order Transaction
{
	orderId: string,
	productId: string,
	quantity: string,
	price: number,
	subTotal: number,
	totalAmount: Number,
	isPaid: Boolean,
	paymentMethod: String,
	datetimeCreated: Date

}

